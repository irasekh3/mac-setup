# mac-setup

## TODO:
- [ ] join chromeos and macos
- [ ] extend chromeos setup to include broader linux
- [ ] add script improvements like `Run :PlugInstall from neovim` with `vim +'PlugInstall' +qa`
- [ ] add `--headless` for quick setup on remote machine
- [ ] support oauth2 with github to get api token and be able to keep repo private ([fetch file from private github repo](https://stackoverflow.com/questions/18126559/how-can-i-download-a-single-raw-file-from-a-private-github-repo-using-the-comman))
- [ ] rewrite in [python](https://janakiev.com/blog/python-shell-commands/) (or [golang](https://golang.org/pkg/os/exec/) if necessary but the impact of python should be minimal as i don't expect to many external libs and most machines come with python already on it - note: kickoff script should still be bash and `$uname -s` or `$uname -a` could be used to identify `darwin` vs `linux`)
- [ ] maybe support linking profile files to git repo to support auto updates and have the profile file run a check for update when loading

## How to Run

This project is used for me to be able to quickly setup a new mac.

run the script by calling:
``` bash
sh -c "$(curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/scripts/configure.sh)"
```

run the zsh configuration script after the system configuration script by calling:
``` bash
zsh -c "$(curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/scripts/zsh_config.sh)"
```
## Manual Configuration Required
* Add ~~Spectacle~~ Rectangle to be able to manage accessibility
* ~~Run :PlugInstall from neovim~~

## SET SYSTEM PREFERENCES LIKE
* dock size, magnification and icons
* highlight color orange
* use dark menu bar and dock
* default browser chrome or brave
* default terminal iterm2
* Background
* keyboard set Caps Lock to escape for apple internal keyboard
* keyboard touchbar lock instead of siri
