#!/usr/bin/env sh -ex

###
# THIS SCRIPT IS INTENDED TO RESET THE STATE OF THE ZSH CONFIG
# RUN THIS BY CALLING
# zsh -c "$(curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/scripts/cleanup/undo_zsh_config.sh)"; source ~/.zshrc
# FROM YOUR COMMAND LINE
###

rm ~/.iterm2_shell_integration.zsh
rm -rf ~/.oh-my-zsh/custom/themes/powerlevel9k
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
rm ~/.iprofile

source ~/.zshrc
