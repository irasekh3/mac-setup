#!/usr/bin/env bash -ex
if [[ ! -z "$(which brew)" ]]; then
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/uninstall)"
fi

personalRepoDir=~/Library/Personal/development/repositories
vimSetupDir=~/Library/Developer/external-code/vim-setup/
externalCodeDir=~/Library/Developer/external-code/
developerDir=~/Library/Developer
personalDir=~/Library/Personal

neovimConfigDir=~/.config/nvim/
dotConfigDir=~/.config/
vimConfigDir=~/.vim/

if [[ -e $personalRepoDir ]]; then
    rm -rf $personalRepoDir
fi

if [[ -e $vimSetupDir ]]; then
    rm -rf $vimSetupDir
fi

if [[ -e $externalCodeDir ]]; then
    rm -rf $externalCodeDir
fi

if [[ -e $developerDir ]]; then
    rm -rf $developerDir
fi

if [[ -e $personalDir ]]; then
    rm -rf $personalDir
fi


if [[ -e $neovimConfigDir ]]; then
    rm -rf $neovimConfigDir
fi

if [[ -e $dotConfigDir ]]; then
    rm -rf $dotConfigDir
fi

if [[ -e $vimConfigDir ]]; then
    rm -rf $vimConfigDir
fi

if [[ -e /usr/local/bin/st ]]; then
    rm -rf /usr/local/bin/st
fi

if [[ -e ~/Library/Fonts/Hack\ Bold\ Nerd\ Font\ Complete.ttf ]]; then
    rm -rf ~/Library/Fonts/Hack\ Bold\ Nerd\ Font\ Complete.ttf
fi

if [[ -e ~/Library/Fonts/Hack\ Bold\ Italic\ Nerd\ Font\ Complete.ttf ]]; then
    rm -rf ~/Library/Fonts/Hack\ Bold\ Italic\ Nerd\ Font\ Complete.ttf
fi

if [[ -e ~/Library/Fonts/Hack\ Italic\ Nerd\ Font\ Complete.ttf ]]; then
    rm -rf ~/Library/Fonts/Hack\ Italic\ Nerd\ Font\ Complete.ttf
fi

if [[ -e ~/Library/Fonts/Hack\ Regular\ Nerd\ Font\ Complete.ttf ]]; then
    rm -rf ~/Library/Fonts/Hack\ Regular\ Nerd\ Font\ Complete.ttf
fi

if [[ -e /usr/local/bin/subl ]]; then
    rm -rf /usr/local/bin/subl
fi

if [[ -e ~/.vimrc ]]; then
    rm -rf ~/.vimrc
fi

if [[ -e ~/.local/share/nvim/ ]]; then
    rm -rf ~/.local/share/nvim/
fi

if [[ -e ~/Library/Application\ Support/Sublime\ Text\ 3/ ]]; then
    rm -rf ~/Library/Application\ Support/Sublime\ Text\ 3/
fi

if [[ -e /Applications/Atom.app ]]; then
    rm -rf /Applications/Atom.app
fi

if [[ -e /Applications/Sublime\ Text.app ]]; then
    rm -rf /Applications/Sublime\ Text.app
fi

if [[ -e /Applications/Visual\ Studio\ Code.app ]]; then
    rm -rf /Applications/Visual\ Studio\ Code.app
fi

if [[ -e /Applications/Android\ Studio.app ]]; then
    rm -rf /Applications/Android\ Studio.app
fi

if [[ -e /Applications/GoLand.app ]]; then
    rm -rf /Applications/GoLand.app
fi

if [[ -e /Applications/PyCharm.app ]]; then
    rm -rf /Applications/PyCharm.app
fi

if [[ -e /Applications/Visual\ Studio.app ]]; then
    rm -rf /Applications/Visual\ Studio.app
fi

if [[ -e /Applications/Xamarin.app ]]; then
    rm -rf /Applications/Xamarin.app
fi

if [[ -e /Applications/Google\ Chrome.app ]]; then
    rm -rf /Applications/Google\ Chrome.app
fi

if [[ -e /Applications/Firefox.app ]]; then
    rm -rf /Applications/Firefox.app
fi

if [[ -e /Applications/GPG\ Keychain.app ]]; then
    rm -rf /Applications/GPG\ Keychain.app
fi

if [[ -e /Applications/iTerm.app ]]; then
    rm -rf /Applications/iTerm.app
fi

if [[ -e /Applications/Postico.app ]]; then
    rm -rf /Applications/Postico.app
fi

if [[ -e /Applications/Postman.app ]]; then
    rm -rf /Applications/Postman.app
fi

if [[ -e /Applications/Spectacle.app ]]; then
    rm -rf /Applications/Spectacle.app
fi

if [[ -e /Applications/VirtualBox.app ]]; then
    rm -rf /Applications/VirtualBox.app
fi

if [[ -e /Applications/WhatsApp.app ]]; then
    rm -rf /Applications/WhatsApp.app
fi

if [[ -e /Applications/Wireshark.app ]]; then
    rm -rf /Applications/Wireshark.app
fi

echo "change shell back to /bin/bash"
chsh -s /bin/bash

if [[ -e ~/.oh-my-zsh ]]; then
    rm -rf ~/.oh-my-zsh
fi

su - $(whoami) && exit 0;
