#!/usr/bin/env sh -ex

###
# RUN THIS BY CALLING
# zsh -c "$(curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/scripts/zsh_config.sh)" 2&>> /tmp/setup.log &
# FROM YOUR COMMAND LINE
###

curl -L https://iterm2.com/shell_integration/zsh -o ~/.iterm2_shell_integration.zsh || true
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
mv ~/.zshrc ~/.zshrc.bu 
curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/oh-my-zsh_custom_themes_powerlevel9k_functions_icons.zsh -o ~/.oh-my-zsh/custom/themes/powerlevel9k/functions/icons.zsh || true
curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/dotfiles/zshrc -o ~/.zshrc || true
curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/dotfiles/iprofile -o ~/.iprofile || true

source ~/.zshrc
