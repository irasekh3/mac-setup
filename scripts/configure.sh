#!/usr/bin/env bash -e

###
# RUN THIS BY CALLING
# sh -c "$(curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/configure.sh)" 2&>> /tmp/setup.log &
# FROM YOUR COMMAND LINE
###

## VARIABLES
SKIP_XCODE_CLI_TOOLS=0
SKIP_HOMEBREW=0

SETUP_DIRECTORIES_DEV=0
SETUP_DIRECTORIES_PERSONAL=0

INSTALL_TEXTEDITOR_ATOM=0
INSTALL_TEXTEDITOR_NEOVIM=0
INSTALL_TEXTEDITOR_SUBLIMETEXT=0
INSTALL_TEXTEDITOR_VIM=0
INSTALL_TEXTEDITOR_VSCODE=0

INSTALL_IDE_ANDROIDSTUDIO=0
INSTALL_IDE_GOLAND=0
INSTALL_IDE_PYCHARM=0
INSTALL_IDE_VISUALSTUDIO=0
INSTALL_IDE_XAMARIN=0

INSTALL_BROWSER_CHROME=0
INSTALL_BROWSER_FIREFOX=0

INSTALL_CLI_CONSUL=0
INSTALL_CLI_FZF=0
INSTALL_CLI_GPG=0
INSTALL_CLI_TERRAFORM=0
INSTALL_CLI_VAULT=0
INSTALL_CLI_WGET=0
INSTALL_CLI_ZSH=0

INSTALL_APP_GPGSUITE=0
INSTALL_APP_ITERM=0
INSTALL_APP_POSTICO=0
INSTALL_APP_POSTMAN=0
INSTALL_APP_SPECTACLE=0
INSTALL_APP_VIRTUALBOX=0
INSTALL_APP_WHATSAPP=0
INSTALL_APP_WIRESHARK=0

INSTALL_FONT_NERDFONT=0

INSTALL_LANGUAGE_GO=0
INSTALL_LANGUAGE_NODEJS=0
INSTALL_LANGUAGE_PYTHON3=0


function silent_xcode_cli_tools_install () {
    if [ '/Library/Developer/CommandLineTools' != "$(xcode-select -p)" ] && [ '/Applications/Xcode.app/Contents/Developer' != "$(xcode-select -p)" ]; then
        touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
        PROD=$(softwareupdate -l |
          grep "\*.*Command Line" |
          tail -n 1 | awk -F"*" '{print $2}' |
          sed -e 's/^ *//' |
          tr -d '\n')
        softwareupdate -i "$PROD" --verbose;
    fi
}

function brew_install () {
    formula=$1
    cask=$2
    pass=$3

    if [ ! -z $(brew ls | grep -i "${formula}") ] || [ ! -z $(brew cask ls  | grep -i "${formula}") ]; then
      echo "${formula} already installed"
    else
        echo "installing ${formula}"
        if [[ true = ${cask} ]]; then
            # if [[ -z ${pass} ]]; then
            #     brew cask install ${formula}
            # else
            #     echo "${pass}" | sudo -S brew cask install ${formula}
            # fi
            brew cask install ${formula}
        else
            brew install ${formula}
        fi
        echo "${formula} installed"
    fi
}

function default_setup () {
    export SETUP_DIRECTORIES_DEV=1
    export SETUP_DIRECTORIES_PERSONAL=1
    export INSTALL_TEXTEDITOR_NEOVIM=1
    export INSTALL_TEXTEDITOR_SUBLIMETEXT=1
    export INSTALL_BROWSER_CHROME=1
    export INSTALL_BROWSER_FIREFOX=1
    export INSTALL_CLI_FZF=1
    export INSTALL_CLI_GPG=1
    export INSTALL_CLI_WGET=1
    export INSTALL_CLI_ZSH=1
    export INSTALL_APP_ITERM=1
    export INSTALL_APP_SPECTACLE=1
    export INSTALL_APP_WHATSAPP=1
    export INSTALL_FONT_NERDFONT=1
    export INSTALL_LANGUAGE_GO=1
    export INSTALL_LANGUAGE_NODEJS=1
    export INSTALL_LANGUAGE_PYTHON3=1

    # UNDECIDED
    export INSTALL_APP_POSTICO=1
    export INSTALL_APP_POSTMAN=1
    # export INSTALL_APP_WIRESHARK=1
}

function full_setup () {
    export SETUP_DIRECTORIES_DEV=1
    export SETUP_DIRECTORIES_PERSONAL=1
    export INSTALL_TEXTEDITOR_ATOM=1
    export INSTALL_TEXTEDITOR_NEOVIM=1
    export INSTALL_TEXTEDITOR_SUBLIMETEXT=1
    export INSTALL_TEXTEDITOR_VIM=1
    export INSTALL_TEXTEDITOR_VSCODE=1
    export INSTALL_IDE_ANDROIDSTUDIO=1
    export INSTALL_IDE_GOLAND=1
    export INSTALL_IDE_PYCHARM=1
    export INSTALL_IDE_VISUALSTUDIO=1
    export INSTALL_IDE_XAMARIN=1
    export INSTALL_BROWSER_CHROME=1
    export INSTALL_BROWSER_FIREFOX=1
    export INSTALL_CLI_CONSUL=1
    export INSTALL_CLI_FZF=1
    export INSTALL_CLI_GPG=1
    export INSTALL_CLI_TERRAFORM=1
    export INSTALL_CLI_VAULT=1
    export INSTALL_CLI_WGET=1
    export INSTALL_CLI_ZSH=1
    export INSTALL_APP_GPGSUITE=1
    export INSTALL_APP_ITERM=1
    export INSTALL_APP_POSTICO=1
    export INSTALL_APP_POSTMAN=1
    export INSTALL_APP_SPECTACLE=1
    export INSTALL_APP_VIRTUALBOX=1
    export INSTALL_APP_WHATSAPP=1
    export INSTALL_APP_WIRESHARK=1
    export INSTALL_FONT_NERDFONT=1
    export INSTALL_LANGUAGE_GO=1
    export INSTALL_LANGUAGE_NODEJS=1
    export INSTALL_LANGUAGE_PYTHON3=1
}

function handle_setup {
    ## SETUP DIRECTORIES
    developerDir=~/Library/Developer
    personalRepoDir=~/Library/Personal/development/repositories
    externalCodeDir=~/Library/Developer/external-code/
    vimSetupDir=~/Library/Developer/external-code/vim-setup/

    if [ $SETUP_DIRECTORIES_DEV = 1 ] && [ ! -e $developerDir ]; then
        mkdir ~/Library/Developer
    fi

    if [[ $SETUP_DIRECTORIES_PERSONAL = 1 ]]; then
        if [[ ! -e $personalRepoDir ]]; then
            mkdir -p $personalRepoDir
        fi
        if [[ $SETUP_DIRECTORIES_DEV = 1 ]]; then
            ln -fs $personalRepoDir $developerDir/personal-repo
        fi
    fi

    if [[ ! -e $externalCodeDir ]]; then
        mkdir -p $externalCodeDir
    fi

    if [[ ! -e $vimSetupDir ]]; then
        mkdir -p $vimSetupDir
    fi

    ## HANDLE XCODE DEPENDENCY
    if [[ $SKIP_XCODE_CLI_TOOLS = 0 ]]; then
        silent_xcode_cli_tools_install;
    fi
    
    if [[ $SKIP_HOMEBREW = 0 ]]; then
        ## BREW
        if [[ -z "$(which brew)" ]]; then
            echo "installing homebrew"
            echo | /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
            echo "homebrew installed"
        else
            echo "hombrew already installed"
        fi
    fi

    ## INSTALLATION FOR CLI TOOLS
    if [[ $INSTALL_CLI_CONSUL = 1 ]]; then
        brew_install "consul"
    fi

    if [[ $INSTALL_CLI_FZF = 1 ]]; then
        brew_install "fzf"
        $(brew --prefix)/opt/fzf/install --all
    fi

    if [[ $INSTALL_CLI_GPG = 1 ]]; then
        brew_install "gnupg"
        brew_install "yubikey-personalization"
        brew_install "hopenpgp-tools"
        brew_install "ykman"
        brew_install "pinentry-mac"
    fi

    if [[ $INSTALL_CLI_TERRAFORM = 1 ]]; then
        brew_install "terraform"
    fi

    if [[ $INSTALL_CLI_VAULT = 1 ]]; then
        brew_install "vault"
    fi

    if [[ $INSTALL_CLI_WGET = 1 ]]; then
        brew_install "wget"
    fi

    ## INSTALLATION FOR TEXT EDITORS
    if [[ $INSTALL_TEXTEDITOR_ATOM = 1 ]]; then
        brew_install "atom" true "${usrPass}";
    fi

    if [[ $INSTALL_TEXTEDITOR_NEOVIM = 1 ]]; then
        # NeoVim
        brew_install "neovim"
        # configs
        if [[ ! -e ~/.config/nvim/ ]]; then
            mkdir -p ~/.config/nvim/
        fi
        if [[ ! -e ${vimSetupDir} ]]; then
            git clone https://github.com/flazz/vim-colorschemes.git ${vimSetupDir}/vim-colorschemes
        fi
        ln -fs ${vimSetupDir}/vim-colorschemes/colors ~/.config/nvim/colors
        curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/dotfiles/vimrc -o ~/.config/nvim/init.vim || true
        # vim-plug for plugins
        curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        if [[ $INSTALL_CLI_FZF = 0 ]]; then
            brew_install "fzf"
            $(brew --prefix)/opt/fzf/install --all
        fi

        pip2 install neovim
        pip3 install neovim

        # setup vim plugins
        vim +'PlugInstall' +qa
    fi

    if [[ $INSTALL_TEXTEDITOR_SUBLIMETEXT = 1 ]]; then
        brew_install "sublime-text" true "${usrPass}"
        
        ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/st || true
        ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl || true
        mkdir -p ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/ || true
        wget https://gitlab.com/irasekh3/mac-setup/raw/master/sublime-text-config/Preferences.sublime-settings -O ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/Preferences.sublime-settings || true
        wget https://gitlab.com/irasekh3/mac-setup/raw/master/sublime-text-config/Preferences.sublime-keymap -O ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/Default\ \(OSX\).sublime-keymap || true

    fi

    if [[ $INSTALL_TEXTEDITOR_VIM = 1 ]]; then
        # vim
        mkdir ~/.vim
        if [[ $INSTALL_TEXTEDITOR_NEOVIM = 1 ]]; then
            ln -s ~/.config/nvim/init.vim ~/.vimrc
            ln -s ~/.config/nvim/colors ~/.vim/colors
            ln -s ~/.config/nvim/scripts ~/.vim/scripts
        else
            if [[ ! -e ${vimSetupDir} ]]; then
                git clone https://github.com/flazz/vim-colorschemes.git ${vimSetupDir}/vim-colorschemes
            fi
            ln -fs ${vimSetupDir}/vim-colorschemes/colors ~/.vim/colors
            ln -fs ${vimSetupDir}/vim-colorschemes/scripts ~/.vim/scripts
            curl -fsSL https://gitlab.com/irasekh3/mac-setup/raw/master/dotfiles/vimrc -o ~/.vimrc
        fi
    fi

    if [[ $INSTALL_TEXTEDITOR_VSCODE = 1 ]]; then
        brew_install "visual-studio-code" true "${usrPass}";
    fi

    ## INSTALLATION FOR IDEs
    if [[ $INSTALL_IDE_ANDROIDSTUDIO = 1 ]]; then
        brew_install "android-studio" true "${usrPass}";
    fi

    if [[ $INSTALL_IDE_GOLAND = 1 ]]; then
        brew_install "goland" true "${usrPass}";
    fi

    if [[ $INSTALL_IDE_PYCHARM = 1 ]]; then
        brew_install "pycharm" true "${usrPass}";
    fi

    if [[ $INSTALL_IDE_VISUALSTUDIO = 1 ]]; then
        brew_install "visual-studio" true "${usrPass}";
    fi

    if [[ $INSTALL_IDE_XAMARIN = 1 ]]; then
        brew_install "xamarin" true "${usrPass}";
    fi

    ## INSTALLATION FOR BROWSERS
    if [[ $INSTALL_BROWSER_CHROME = 1 ]]; then
        brew_install "google-chrome" true "${usrPass}"
    fi

    if [[ $INSTALL_BROWSER_FIREFOX = 1 ]]; then
        brew_install "firefox" true "${usrPass}"
    fi

    ## INSTALLATION OF OTHER APPS
    if [[ $INSTALL_APP_GPGSUITE = 1 ]]; then
        brew_install "gpg-suite" true "${usrPass}"
    fi

    if [[ $INSTALL_APP_ITERM = 1 ]]; then
        brew_install "iterm2" true "${usrPass}"
        echo "Don't forget to setup iTerm:"
        echo "colors"
        echo "profiles"
        echo "key bindings"
        echo "save state on close"
    fi

    if [[ $INSTALL_APP_POSTICO = 1 ]]; then
        brew_install "postico" true "${usrPass}"
    fi

    if [[ $INSTALL_APP_POSTMAN = 1 ]]; then
        brew_install "postman" true "${usrPass}"
    fi

    if [[ $INSTALL_APP_SPECTACLE = 1 ]]; then
        brew_install "spectacle" true "${usrPass}"
        ## ADD SPECTACLE AS AN ACCESSIBILITY
        ## COMMENTED OUT BECAUSE THIS IS NOW LOCKED DOWN BY SIP
        ## echo "${pass}" | sudo -S sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db "INSERT or REPLACE INTO access VALUES('kTCCServiceAccessibility','com.divisiblebyzero.Spectacle',0,1,1,NULL)"
    fi

    if [[ $INSTALL_APP_VIRTUALBOX = 1 ]]; then
        brew_install "virtualbox" true "${usrPass}"
        brew_install "virtualbox-extension-pack" true "${usrPass}"
    fi

    if [[ $INSTALL_APP_WHATSAPP = 1 ]]; then
        brew_install "whatsapp" true "${usrPass}"
    fi

    if [[ $INSTALL_APP_WIRESHARK = 1 ]]; then
        brew_install "wireshark" true "${usrPass}"
    fi

    # INSTALLATION FOR FONTS
    if [[ $INSTALL_FONT_NERDFONT = 1 ]]; then
        brew tap caskroom/fonts
        brew_install "font-hack-nerd-font" true "${usrPass}" || true
    fi

    ## INSTALLATION FOR LANGUAGES
    if [[ $INSTALL_LANGUAGE_GO = 1 ]]; then
        brew_install "go"
    fi

    if [[ $INSTALL_LANGUAGE_NODEJS = 1 ]]; then
        brew_install node
        brew_install watchman
        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
        npm install npm@latest -g
        npm install -g create-react-native-app
    fi

    if [[ $INSTALL_LANGUAGE_PYTHON3 = 1 ]]; then
        ## NOT YET IMPLEMENTED
        echo "NOT YET IMPLEMENTED"
    fi

    # ALWAYS HANDLE OH MY ZSH LAST BECAUSE THIS QUITS THE CURRENT SCRIPT
    if [[ $INSTALL_CLI_ZSH = 1 ]]; then
        ##
        # OH-MY-ZSH
        # copy .bash_history to .zsh_histpry
        # then finish with zsh becuase it kills the process when changing shell
        ##
        /usr/bin/ruby -e "$(curl -fsSL https://gist.githubusercontent.com/goyalankit/a1c88bfc69107f93cda1/raw/ed58ae6b8929021353001b1826694780c033291a/bash_to_zsh_history.rb)"
        # remove the setting of the env var for the shell 
        # sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sed '/\s*env\s\s*zsh\s*/d')"
    fi
}

function usage()
{
    printf "Usage: $package [options]\n"
    printf " -h, --help             Show usage\n"
    printf " -d, --default          Setup default configuration\n"
    printf " -a, --all              Setup full configuration\n"
    # printf " -i, --conf             Set gpg.conf and gpg-agent.conf to\n"
    # printf "                        recommened defaults\n"

}

function parse_options () {
    full_flag=0
    default_flag=0
    any_flag=0

    while test $# -gt 0; do
        case "$1" in
            -h|--help)
                usage
                exit 1;
                ;;

            -d|--default)
                if [[ $full_flag = 0 ]]; then
                    default_setup
                    default_flag=1
                else
                    printf "Conflicting Options: cannot setup both full and default settings. Please select one.\n\n"
                    usage
                    exit 1;
                fi

                any_flag=1
                shift
                ;;

            -a|--all)
                if [[ $default_flag = 0 ]]; then
                    full_setup
                    full_flag=1
                else
                    printf "Conflicting Options: cannot setup both full and default settings. Please select one.\n\n"
                    usage
                    exit 1;
                fi

                any_flag=1
                shift
                ;;

            *)
                printf "Invalid Option: $1\n\n"
                usage
                exit 1;
                ;;
        esac
    done

    # If no flags set to default
    if [[ $any_flags -gt 0 ]]; then 
        default
        echo "nothing to handle"
        exit 0;
    fi
}

parse_options $*;

# echo -n "Password: "
# read -s pass
# usrPass=${pass}

handle_setup;

### SET SYSTEM PREFERENCES LIKE
## dock size, magnification and icons
## highlight color orange
## use dark menu bar and dock
## default browser chrome
## default terminal iterm2
## Background
## keyboard touchbar lock instead of siri

exit 0;
